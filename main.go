package main

import (
	"bitbucket.org/swigy/hudor/configparsers"
	"bitbucket.org/swigy/hudor/lib"
	"fmt"
	"math/rand"
	"time"
)

func main() {

	testRequest:= func(taskObj configparsers.Task) {
		fmt.Println(time.Now())
		source:=rand.NewSource(time.Now().UnixNano())
		time.Sleep(time.Duration(rand.New(source).Intn(100) + 200) * time.Millisecond)
	}


		hudorObj,err:= lib.Init("profile/tasks.json")
		if err != nil {
			fmt.Println(err.Error())
		}
		hudorObj.Run("TestRequest",testRequest)
		time.Sleep(10*time.Second)
		hudorObj.UpdateTaskRate("TestRequest",5)
		time.Sleep(10 * time.Second)
		hudorObj.UpdateTaskRate("TestRequest",3)
		time.Sleep(10 * time.Second)
		hudorObj.UpdateTaskRate("TestRequest",1)
		time.Sleep(10 * time.Second)
		hudorObj.UpdateTaskRate("TestRequest",2)
		time.Sleep(10 * time.Second)
		hudorObj.UpdateTaskRate("TestRequest",1)
		hudorObj.WaitForAllTasksToComplete()
		fmt.Println("done")
}
