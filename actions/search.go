package actions

import (
	"bitbucket.org/swigy/hudor-client/constants"
	"bitbucket.org/swigy/hudor/configparsers"
	"bitbucket.org/swigy/hudor/genericUtilities"
	"bitbucket.org/swigy/hudor/redis"
	"bitbucket.org/swigy/hudor/request"
	"fmt"
	"github.com/fatih/color"
	"strings"
)

var SearchRequest = func(taskObj configparsers.Task) {
	redisValue,redisErr := redis.GetSessionData(taskObj.GetRedisConnectionByName("session-data"))

	if redisValue != "" && redisErr == nil {
		latlong, _ := taskObj.GetRandomValueFromFile("latlong")
		searchText,_ := taskObj.GetRandomValueFromFile("search")

		url := constants.APP_URL + taskObj.Object.Httpendpoint.URL
		url = replaceLatLongSearchTextInURL(url, latlong[0], latlong[1], searchText[0])

		headers := taskObj.Object.Httpendpoint.GetHeaders()
		headers = genericUtilities.ReplaceTidSidTokenInHeadersFromJson(headers, redisValue)

		respBytes, _, _ := request.MakeHTTPRequest(url,
			taskObj.Object.Httpendpoint.Method, headers, "")

		fmt.Println(string(respBytes))

	} else {
		color.Red(taskObj.Name + " ---> not able to fetch value from redis ")
	}

}

func replaceLatLongSearchTextInURL(url string, values ...string) string {
	url = strings.ReplaceAll(url,"${lat}", values[0])
	url = strings.ReplaceAll(url, "${long}", values[1])
	searchText:=strings.ReplaceAll(values[2], " ","%20")
	url = strings.ReplaceAll(url, "${searchtext}", searchText)
	return url;
}